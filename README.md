Project
================

### Initial setup:
```
git clone git@gitlab.com:tomislav.parcina/ezora.git tomislav_parcina_ezora
cd tomislav_parcina_ezora
docker-compose up -d
docker-compose exec php composer install
```
### Database setup:
```
docker-compose exec db mysql -uroot -proot -e "CREATE DATABASE db_test;"
docker-compose exec php php bin/console --no-interaction doctrine:migrations:migrate
docker-compose exec php php bin/console --no-interaction doctrine:migrations:migrate --env test
bin/console hautelook:fixtures:load --no-interaction
```
### Run tests:
```
docker-compose exec php bin/phpunit tests
```
### URL's
Adminer: http://localhost:8082/?server=db&username=root password: root  
Documentation: http://localhost/docs

All three tasks can be tested by visiting documentation URL.
### Task 01: Math Solver Class
src/Service/MAthSolver.php  
For new operators, only add new method that implements that operator. No changes to existing code or logic is needed. 
### Task 02: DB Structure
Formulation of this task (Demonstrate using PHP how you would connect to a MySQL or PostgreSQL database and query for a specific record) indicates that is expected to initate DB connection from code. But that is not necessary when using Controller, DQL and anotations. Symfony framework does all "havy" work for me, and I get User in Controller as input parameter.

Index on id_user field is configured trough entity anotation: @ORM\Table(name="users",indexes={@ORM\Index(name="search_id_user", columns={"id_user"})})
### Task 03. Thai National Lottery draw
