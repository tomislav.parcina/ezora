<?php

declare(strict_types=1);

namespace App\Controller\Api;

use \App\Service\NextLotteryDraw as NextLotteryDrawService;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class NextLotteryDraw
{
    private SerializerInterface $serializer;
    private NextLotteryDrawService $nextLotteryDraw;

    public function __construct(
        SerializerInterface $serializer,
        NextLotteryDrawService $nextLotteryDraw
    ) {
        $this->serializer = $serializer;
        $this->nextLotteryDraw = $nextLotteryDraw;
    }

    /**
     * @Route(path="/nextLotteryDraw", methods={"GET"}, name="next_lottery_draw")
     * @SWG\Get(
     *   tags={"ezora"},
     *   summary="Retrieve a next lottery draw",
     * )
     * @SWG\Parameter(
     *   name="referentDate",
     *   in="query",
     *   type="string",
     *   default="2021-12-25 08:13:47",
     *   description="Referent date-time for next lottery draw calculation (empty = current date-time)",
     *   required=false,
     * )
     * @SWG\Response(
     *   response=200,
     *   description="Next lottery draw date and time",
     *   @SWG\Schema(
     *     @SWG\Property(
     *       property="nextDateTime",
     *       description="Next lottery draw date and time",
     *       type="string"
     *     ),
     *   ),
     * )
     * @SWG\Response(
     *   response=404,
     *   description="Short URL not found",
     * )
     */
    public function __invoke(Request $request): JsonResponse
    {
        $nextLotteryDrawDate['nextDateTime'] = $this->nextLotteryDraw->getNextLotteryDrawDate($request);
        $data = $this->serializer->serialize($nextLotteryDrawDate, 'json');

        return new JsonResponse($data, JsonResponse::HTTP_OK, [], true);
    }
}
