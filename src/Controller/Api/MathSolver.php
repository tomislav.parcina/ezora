<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Dto\MathTaskDto;
use \App\Service\MathSolver as MathSolverService;
use Exception;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class MathSolver
{
    private SerializerInterface $serializer;
    private MathSolverService $mathSolverService;

    public function __construct(
        SerializerInterface $serializer,
        MathSolverService $mathSolverService)
    {
        $this->serializer = $serializer;
        $this->mathSolverService = $mathSolverService;
    }

    /**
     * @Route(path="/mathSolver", methods={"GET"}, name="math_solver")
     * @SWG\Get(
     *   tags={"ezora"},
     *   summary="Solve a math calculation",
     * )
     * @SWG\Parameter(
     *   name="operandOne",
     *   in="query",
     *   type="string",
     *   default="6",
     *   description="Value of first operand",
     *   required=true,
     * )
     * @SWG\Parameter(
     *   name="operandTwo",
     *   in="query",
     *   type="string",
     *   default="2",
     *   description="Value of second operand",
     *   required=true,
     * )
     * @SWG\Parameter(
     *   name="operator",
     *   in="query",
     *   type="string",
     *   default="Plus",
     *   description="Operator",
     *   required=true,
     * )
     * @SWG\Response(
     *   response=200,
     *   description="Result of math calculation",
     * )
     * @SWG\Response(
     *   response=400,
     *   description="Request was invalid",
     * )
     */
    public function __invoke(Request $request): JsonResponse
    {
        try {
            $mathTask = new MathTaskDto(
                $request->query->get('operandOne', null),
                $request->query->get('operandTwo', null),
                $request->query->get('operator', null)
            );
            $result = $this->mathSolverService->solve($mathTask);
        } catch (Exception $exception) {
            return new JsonResponse($exception->getMessage(), JsonResponse::HTTP_BAD_REQUEST, [], false);
        }
        $data = $this->serializer->serialize($result, 'json');

        return new JsonResponse($data, JsonResponse::HTTP_OK, [], true);
    }
}
