<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\User;
use Exception;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class GetUser
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route(path="/user/{user}", methods={"GET"}, name="user_get")
     * @SWG\Get(
     *   tags={"ezora"},
     *   summary="Get a user",
     * )
     * @SWG\Parameter(
     *   name="user",
     *   in="path",
     *   type="integer",
     *   description="User ID",
     * )
     * @SWG\Response(
     *   response=200,
     *   description="Requested user",
     *   @Model(type=User::class, groups={"user:get"})
     * )
     * @SWG\Response(
     *   response=404,
     *   description="User not found",
     * )
     */
    public function __invoke(User $user): JsonResponse
    {
        $data = $this->serializer->serialize($user, 'json', ['groups' => 'user:get']);

        return new JsonResponse($data, JsonResponse::HTTP_OK, [], true);
    }
}
