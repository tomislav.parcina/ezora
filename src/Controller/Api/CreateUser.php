<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\User;
use App\Service\UserCreation;
use Exception;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CreateUser
{
    private SerializerInterface $serializer;
    private UserCreation $userCreation;

    public function __construct(
        SerializerInterface $serializer,
        UserCreation $userCreation
    ) {
        $this->serializer = $serializer;
        $this->userCreation = $userCreation;
    }

    /**
     * @Route(path="/user", methods={"POST"}, name="user_create")
     * @SWG\Post(
     *   tags={"ezora"},
     *   summary="Create a new User",
     * )
     * @SWG\Parameter(
     *   name="body",
     *   in="body",
     *   type="json",
     *   description="JSON body",
     *   required=true,
     *   @Model(type=User::class, groups={"user:create"})
     * )
     * @SWG\Response(
     *   response=200,
     *   description="User created",
     *   @Model(type=User::class, groups={"user:get", "user:roles"})
     * )
     * @SWG\Response(
     *   response=400,
     *   description="User not created",
     * )
     */
    public function __invoke(Request $request): JsonResponse
    {
        try {
            $user = $this->userCreation->create($request);
        } catch (Exception $exception) {
            return new JsonResponse($exception->getMessage(), JsonResponse::HTTP_BAD_REQUEST, [], false);
        }
        $data = $this->serializer->serialize($user, 'json', ['groups' => 'user:get']);

        return new JsonResponse($data, JsonResponse::HTTP_OK, [], true);
    }

    private function getLongUrl(Request $request): string
    {
        return json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR)['longUrl'];
    }
}
