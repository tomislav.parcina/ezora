<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;

class UserRepository extends BaseRepository
{
    public const ENTITY_CLASS_NAME = User::class;
}
