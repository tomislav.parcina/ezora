<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="users",indexes={@ORM\Index(name="search_id_user", columns={"id_user"})})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User implements EntityInterface
{
    /**
     * @ORM\Column(name="id_user", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"user:get", "user:id"})
     */
    private int $idUser;

    /**
     * @ORM\Column(name="name", type="string", length=1024, nullable=false)
     * @Groups({"user:get", "user:create", "user:name"})
     */
    private string $name;

    /**
     * @ORM\Column(name="age", type="integer", nullable=false)
     * @Groups({"user:get", "user:create", "user:age"})
     */
    private int $age;

    /**
     * @ORM\Column(name="job_title", type="string", length=1024, nullable=false)
     * @Groups({"user:get", "user:create", "user:jobTitle"})
     */
    private string $jobTitle;

    /**
     * @ORM\Column(name="inserted_on", type="datetime", nullable=false)
     * @Groups({"user:get", "user:create", "user:insertedOn"})
     */
    private DateTime $insertedOn;

    /**
     * @ORM\Column(name="last_updated", type="datetime", nullable=false)
     * @Groups({"user:get", "user:create", "user:lastUpdated"})
     */
    private DateTime $lastUpdated;

    public function getId(): int
    {
        return $this->idUser;
    }

    public function getIdUser(): ?int
    {
        return $this->idUser;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    public function setJobTitle(string $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    public function getInsertedOn(): ?DateTime
    {
        return $this->insertedOn;
    }

    public function setInsertedOn(DateTime $insertedOn): self
    {
        $this->insertedOn = $insertedOn;

        return $this;
    }

    public function getLastUpdated(): ?DateTime
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated(DateTime $lastUpdated): self
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }
}
