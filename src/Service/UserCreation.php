<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Symfony\Component\HttpFoundation\Request;

class UserCreation
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    public function create(Request $request): User
    {
        $requestData = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $user = new User();
        $user->setName($requestData['name'])
            ->setAge($requestData['age'])
            ->setJobTitle($requestData['jobTitle'])
            ->setInsertedOn(new DateTimeImmutable($requestData['insertedOn']))
            ->setLastUpdated(new DateTimeImmutable($requestData['lastUpdated']));
        $this->userRepository->save($user);

        return $user;
    }
}
