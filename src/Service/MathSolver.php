<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\MathTaskDto;
use InvalidArgumentException;

class MathSolver
{
    private const AVAILABLE_METHODS = [
        'plus',
        'minus',
        'times',
        'divide',
    ];

    public function solve(MathTaskDto $mathTask): float
    {
        $operatorMethod = $mathTask->getOperator();
        if (in_array($operatorMethod, self::AVAILABLE_METHODS)) {
            return $this->$operatorMethod($mathTask->getOperandOne(), $mathTask->getOperandTwo());
        }
        throw new InvalidArgumentException(sprintf(
            'Requested method %s is not one of supported methds: %s.',
            $operatorMethod,
            implode(', ', self::AVAILABLE_METHODS)
        ));
    }

    private function plus(float $operandOne, float $operandTwo): float
    {
        return $operandOne + $operandTwo;
    }

    private function minus(float $operandOne, float $operandTwo): float
    {
        return $operandOne - $operandTwo;
    }

    private function times(float $operandOne, float $operandTwo): float
    {
        return $operandOne * $operandTwo;
    }

    private function divide(float $operandOne, float $operandTwo): float
    {
        if ($operandTwo === 0.0) {
            throw new InvalidArgumentException('To perform devide, operand two should not be 0.');
        }

        return $operandOne / $operandTwo;
    }
}
