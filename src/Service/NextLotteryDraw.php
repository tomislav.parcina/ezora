<?php

declare(strict_types=1);

namespace App\Service;

use DateTimeImmutable;
use Symfony\Component\HttpFoundation\Request;

class NextLotteryDraw
{
    public const DRAW_TIME = '21:30:00';

    public function getNextLotteryDrawDate(Request $request): DateTimeImmutable
    {
        $referentDate = $this->getReferentDate($request);
        $dayOfWeek = $referentDate->format('N');
        if ($dayOfWeek === '1') {
            return $this->getNextTuesdayDraw($referentDate);
        }
        if ($dayOfWeek === '2') {
            if ($referentDate < new DateTimeImmutable(
                sprintf('%s %s', $referentDate->format('Y-m-d'), self::DRAW_TIME)
            )){
                return $this->getReferentDateDrawTime($referentDate);
            }

            return $this->getNextSundayDraw($referentDate);
        }
        if (in_array($dayOfWeek, ['3', '4', '5', '6'])) {
            return $this->getNextSundayDraw($referentDate);
        }
        if ($dayOfWeek === '7'
            && $referentDate < new DateTimeImmutable($referentDate->format('Y-m-d').' 21:30:00')
        ){
            return $this->getReferentDateDrawTime($referentDate);
        }

        return $this->getNextTuesdayDraw($referentDate);

    }

    private function getReferentDateDrawTime($referentDate): DateTimeImmutable
    {
        return new DateTimeImmutable(sprintf('%s %s', $referentDate->format('Y-m-d'), self::DRAW_TIME));
    }

    private function getNextTuesdayDraw(DateTimeImmutable $referentDate): DateTimeImmutable
    {
        return $referentDate->modify('next Tuesday 21:30');
    }

    private function getNextSundayDraw(DateTimeImmutable $referentDate)
    {
        return $referentDate->modify('next Sunday 21:30');
    }

    private function getReferentDate(Request $request): DateTimeImmutable
    {
        $referentDate = $request->query->get('referentDate', null);
        if (null === $referentDate) {
            return new DateTimeImmutable();
        }

        return new DateTimeImmutable($referentDate);
    }
}
