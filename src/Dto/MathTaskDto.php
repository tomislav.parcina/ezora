<?php

declare(strict_types=1);

namespace App\Dto;

class MathTaskDto
{
    private float $operandOne;
    private float $operandTwo;
    private string $operator;

    public function __construct(
        float|string $operandOne,
        float|string $operandTwo,
        string $operator
    ) {
        $this->operandOne = (float) $operandOne;
        $this->operandTwo = (float) $operandTwo;
        $this->operator = strtolower(trim($operator));
    }

    public function getOperandOne(): float
    {
        return $this->operandOne;
    }

    public function getOperandTwo(): float
    {
        return $this->operandTwo;
    }

    public function getOperator(): string
    {
        return $this->operator;
    }
}
