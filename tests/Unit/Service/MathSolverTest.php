<?php

namespace App\Tests\Unit\Service;

use App\Dto\MathTaskDto;
use App\Service\MathSolver;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MathSolverTest extends KernelTestCase
{
    private static MathSolver $mathSolverService;

    public static function setUpBeforeClass(): void
    {
        self::bootKernel();
        $container = static::getContainer();

        /** @var MathSolver $mathSolverService */
        $mathSolverService = $container->get(MathSolver::class);
        self::$mathSolverService = $mathSolverService;
    }

    /**
     * @dataProvider provider
     */
    public function testSolveShouldReturnResult(MathTaskDto $inputValues, float $result): void
    {
        $calculatedResult = self::$mathSolverService->solve($inputValues);
        self::assertSame(
            $result,
            $calculatedResult,
            sprintf('Calculated result "%s" is not as expected "%s".', $calculatedResult, $result)
        );
    }

    public function provider(): array
    {
        return [
            [new MathTaskDto(1, 2, 'plus'), 3.0],
            [new MathTaskDto(5, '4', 'plus'), 9.0],
            [new MathTaskDto('7', 12, 'plus'), 19.0],
            [new MathTaskDto('33', '24', 'plus'), 57.0],
            [new MathTaskDto(1.234, 2.222, 'plus'), 3.456],
            [new MathTaskDto('5.111', '3.222', 'plus'), 8.333],
            [new MathTaskDto(51, 5, 'minus'), 46.0],
            [new MathTaskDto(73, '12', 'minus'), 61.0],
            [new MathTaskDto('94', 5, 'minus'), 89.0],
            [new MathTaskDto('44', '8', 'minus'), 36.0],
            [new MathTaskDto(3, 7, 'times'), 21.0],
            [new MathTaskDto(4, '8', 'times'), 32.0],
            [new MathTaskDto('44', 2, 'times'), 88.0],
            [new MathTaskDto('7', '8', 'times'), 56.0],
            [new MathTaskDto(10, 2, 'divide'), 5.0],
            [new MathTaskDto(24, '8', 'divide'), 3.0],
            [new MathTaskDto('7', 1, 'divide'), 7.0],
            [new MathTaskDto('44', '4', 'divide'), 11.0],
        ];
    }
}
