<?php

namespace App\Tests\Unit\Service;

use App\Service\NextLotteryDraw;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

class NextLotteryDrawTest extends KernelTestCase
{
    private static NextLotteryDraw $nextLotteryDrawService;

    public static function setUpBeforeClass(): void
    {
        self::bootKernel();
        $container = static::getContainer();

        /** @var NextLotteryDraw $nextLotteryDrawService */
        $nextLotteryDrawService = $container->get(NextLotteryDraw::class);
        self::$nextLotteryDrawService = $nextLotteryDrawService;
    }

    /**
     * @dataProvider provider
     */
    public function testGetNextLotteryDrawDateShouldReturnResult(Request $request, string $nextDrawDate): void
    {
        $calculatedNextDrawDate = self::$nextLotteryDrawService->getNextLotteryDrawDate($request);
        self::assertSame(
            $nextDrawDate,
            $calculatedNextDrawDate->format('Y-m-d H:i:s'),
            sprintf('Calculated result "%s" is not as expected "%s".', $calculatedNextDrawDate->format('Y-m-d H:i:s'), $nextDrawDate)
        );
    }

    public function provider(): array
    {
        return [
            [new Request(['referentDate' => '2021-12-13 11:48:21']), '2021-12-14 21:30:00'],
            [new Request(['referentDate' => '2021-12-13 21:30:00']), '2021-12-14 21:30:00'],
            [new Request(['referentDate' => '2021-12-13 21:48:21']), '2021-12-14 21:30:00'],
            [new Request(['referentDate' => '2021-12-14 16:48:21']), '2021-12-14 21:30:00'],
            [new Request(['referentDate' => '2021-12-14 21:30:00']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-14 21:48:21']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-15 16:48:21']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-15 21:30:00']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-15 21:48:21']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-16 16:48:21']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-16 21:30:00']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-16 21:48:21']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-17 16:48:21']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-17 21:30:00']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-17 21:48:21']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-18 16:48:21']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-18 21:30:00']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-18 21:48:21']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-19 16:48:21']), '2021-12-19 21:30:00'],
            [new Request(['referentDate' => '2021-12-19 21:30:00']), '2021-12-21 21:30:00'],
            [new Request(['referentDate' => '2021-12-19 21:48:21']), '2021-12-21 21:30:00'],
        ];
    }
}
